### Log
- log.i - info; log.e - error; log.w - warning
- log.i(key,value)
- logcat查找：i类型：I/key
### Timber：
无需添加信息标签，自动使用该类名称
- website： https://github.com/JakeWharton/timber#download
- 创建Application类，初始化Timber库
```
class ClickerApplication: Application() {
    override fun onCreate() {
        super.onCreate()
        //初始化Timber库
        Timber.plant(Timber.DebugTree())
    }
}
```
- 在androidManifest.xml中加入自定义的application库
```
<application
        android:name=".ClickerApplication"
```
### State:
- onPause：该应用不会再获得焦点。当应用在屏幕上显示且完全具有用户焦点时被称为“交互式生命周期”。部分可见但该activity没有焦点时，会调用onPause，但不会调用onStop
- onStop：该应用不再显示到屏幕上
- onRestart：在activity可见之前调用，onCreate() 方法只在第一次被调用，之后会调用 onRestart()
#### fragment状态：
- onAttach：与activity关联
- onCreate()：初始fragment创建->onCreateView()膨胀fragment布局->onViewCreated()->onStart()可见
- 去下一个Fragment将会导致前一个fragment：onPause()->onStop()->onDestroyView()
- 从别的Fragment返回：由于前一个Fragment对象依然存在并且与Activity关联，因此只用膨胀布局onCreateView开始即可
- 从当前Fragment到后台：只会调用onPause和Onstop，不会销毁fragment布局
- 从后台到当前fragment：只会调用onStart和onResume，不用onCreateView
### 生命周期库
- 生命周期所有者：Activity/fragment，会实现lifecycleOwner接口<br>
生命周期所有者需要向处理函数传递保存自身状态的lifecycle
```
dessert = DessertTimer(this.lifecycle)
```
- Lifecycle类：保存生命周期所有者的实际状态，并触发事件
- 生命周期观察器：实现LifecycleObserver接口
为传入的lifecycle添加观察器：
```
init {
   lifecycle.addObserver(this)
}
```
并给要在onStart/Stop……等状态触发操作的函数加上注解：
```
@OnLifecycleEvent(Lifecycle.Event.ON_STOP)
```
### 保存数据
onSavaInstanceState()
- 在onStop之后onDestroy之前，因为此时应用被转入后台运行，而没有被销毁
- bundle：键值对的集合。
使用：`bundle.putInt(key,value)`或`bundle.putString(key,value)`存储参数
- bundle会保存在RAM中，因此存储的数据应远远小于100k
- 在onCreate处会接收bundle对象，如果重新启动将会把存储好的bundle传入，可以恢复数据：
```
if (savedInstanceState !=null){
    revenue = savedInstanceState.getInt(KEY_REVENUE,0)
    dessertsSold = savedInstanceState.getInt(KEY_DESSERT_SOLD,0)
    dessertTimer.secondsCount = savedInstanceState.getInt(KEY_TIMER_SECONDS,0)
}
```