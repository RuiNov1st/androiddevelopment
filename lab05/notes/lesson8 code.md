### 应用架构
- 界面控制器：显示数据、与用户交互
- ViewModel：处理数据，使数据不会像销毁 activity 或 fragment 实例一样将其销毁
#### ViewModel使用：
- 添加依赖：
```
implementation 'androidx.lifecycle:lifecycle-viewmodel-ktx:2.3.1'
```
- 定义viewModel类，继承ViewModel
```
class GameViewModel : ViewModel() {
}
```
- 在界面控制器使用viewModel：
```
private val viewModel: GameViewModel by viewModels()
binding.textViewUnscrambledWord.text = viewModel.currentScrambledWord
```
使用委托：在配置更改时不会丢失viewModel状态，已经将对象的责任委托给了 delegate 类 viewModels进行处理。
- 数据在viewModel中私有，界面控制器使用时只能只读，改为后备数据：
```
private var _currentScrambledWord = "test"
val currentScrambledWord: String
   get() = _currentScrambledWord
```
### 对话框：
- 构建：
```
private fun showFinalScoreDialog() {
   MaterialAlertDialogBuilder(requireContext())
}
```
### LiveData：
在生命周期所有者的整个生命周期内，当 ViewModel内添加LiveData的值更改时，就会触发新观察器
- MutableLiveData：存储的数据值可更改，但本身对象值不可更改
- 使用LiveData对象中的数据用`LiveData.value`

### 数据绑定
- 视图绑定：绑定类的实例包含对应布局中具有ID的所有视图的直接引用 binding.
- 数据绑定：将数据从代码绑定到视图+将视图绑定到代码
- 数据绑定使用：build.gradle(Module)：`buildFeatures {
   dataBinding = true
}`
- 将布局文件改为Databinding格式：
 Show Context Actions > Convert to data binding layout
- 

### Appendix:
- 使用plus()函数增加值确保null安全
`_score.value = (_score.value)?.plus(SCORE_INCREASE)`
- 使用inc()函数增加1确保null安全
`_currentWordCount.value = (_currentWordCount.value)?.inc()`