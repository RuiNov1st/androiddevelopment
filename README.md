# Android Development

韦诗睿 1913184 NKU-CS

Contact me: ruinovscorp@163.com

## Miniprogram Project
**Updated on Jun13rd**<br>
**Finished!!!**

### 迭代一
- notableDay介绍文档（在线版本）：https://nankai.feishu.cn/docs/doccnYVU26KlpTcuxUrWdlWu8qe
- 迭代一小程序视频演示（在线版本）：https://nankai.feishu.cn/file/boxcn8pnccPg9vsb4VK0xA4ReYe
- 迭代一小程序appid（在线版本）：https://nankai.feishu.cn/file/boxcnxZ6KF7bPRDuL5TsTmf22Ue

### 迭代二
- notableDay介绍文档（在线版本）：https://nankai.feishu.cn/docs/doccnvIF8Lo0FYKNu2a22EeDBHd
- 迭代二小程序视频演示（在线版本）：https://nankai.feishu.cn/file/boxcnrtDmDel6BeoxjlXg8OJgad
- 迭代二小程序appid（在线版本）：https://nankai.feishu.cn/file/boxcnfHwkeyb5IAGv7E0OEePAvi

### 迭代三
- notableDay介绍文档（在线版本）：https://nankai.feishu.cn/docs/doccnSibUyIX6dBu6jKp1TsRQBf#
- 迭代三小程序视频演示（在线版本）：https://nankai.feishu.cn/file/boxcnBzQjDUzhMmZmCwd1mskrys
- 迭代三小程序appid（在线版本）：https://nankai.feishu.cn/file/boxcnq8ukjBtEOQMfAmSJw7tDme
  
### 迭代四
- notableDay介绍文档（在线版本）：https://nankai.feishu.cn/docx/doxcnvpZElYXGT2JaRqnSbfTuDc
- 迭代四小程序视频演示（在线版本）：https://nankai.feishu.cn/file/boxcn0knkLEN2xHR215tSFJe9Tg
- 迭代四小程序appid（在线版本）：https://nankai.feishu.cn/file/boxcnjZd4S1NhjCtJNVWNDuGRrw
---
## Course Assignments
**Finished!!**
### lab01 
Updated on March7th
##### Lesson 1: Kotlin Basics
- Website: https://developer.android.com/courses/pathways/android-development-with-kotlin-1
##### Lesson 2：Functions
- Website:
  https://developer.android.com/courses/pathways/android-development-with-kotlin-2
- notes: Lesson2 code.md
##### Lesson3：Classes and Objects
- Website：
  https://developer.android.com/courses/pathways/android-development-with-kotlin-3
- notes: Lesson3 code.md
##### Lesson4：Build your first Android app
- Website：
  https://developer.android.com/courses/pathways/android-development-with-kotlin-4
- notes: Lesson4 code.md
### lab02: Layouts
Updated on March 9th
- website: https://developer.android.com/courses/pathways/android-development-with-kotlin-5
- notes: Lesson5 code.md & Lab02 recyclerView Note.pdf
### lab03: App navigation
Updated on March 16th
- website:https://developer.android.com/courses/pathways/android-development-with-kotlin-6
- notes: lab03 code.md & lab03 fragment navigation.pdf
### lab04: Activity and fragment lifecycles
Updated on March 28th
- website:https://developer.android.com/courses/pathways/android-development-with-kotlin-7
- notes: lesson7 code.md
### lab05: Lesson 8: App architecture (UI layer)
Updated on March 30th
- website:https://developer.android.com/courses/pathways/android-development-with-kotlin-8
- notes: lesson8 code.md & LiveData note.pdf
### lab06: Lesson 9: App architecture (persistence)
Updated on April 12nd
- website:https://developer.android.com/courses/pathways/android-development-with-kotlin-9
- notes: lesson9 code.md
### lab07: Lesson 10: Advanced RecyclerView use cases
Updated on April 23rd
- website:https://developer.android.com/courses/pathways/android-development-with-kotlin-10
- notes: RecyclerView.pdf
### lab08: Lesson 11: Connect to the internet
Updated on April 27th
- website:https://developer.android.com/courses/pathways/android-development-with-kotlin-11
### lab09: Lesson 12：Repository pattern and WorkManager
Updated On May 9th
- website:https://developer.android.com/courses/pathways/android-development-with-kotlin-12
### lab10: Lesson 13: App UI design
Updated on May 13rd
- website:https://developer.android.com/courses/pathways/android-development-with-kotlin-13
