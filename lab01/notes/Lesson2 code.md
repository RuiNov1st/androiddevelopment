### main()
main函数为kotlin文件入口

    fun main(args:Array<String>){
    println("Hello World!")
    }

如果不使用参数，可以不定义args<br>
参数的设置为 参数名：参数类型 (=默认值) 

    fun swim(speed:String = "fast"){}
在调用有默认值的参数时，可以采用位置参数或者默认值参数的方式调用：<br>
位置参数:`swim("slow")`<br>
默认值参数：`swim(speed = "slow")`<br>
首先放置没有默认值的参数，然后再放置有默认值的参数

### function
一定会返回东西，像python<br>
但是如果不显式指定 ，则会返回Unit/Kotlin.Unit<br>
Unit is a type with only one value: Unit.<br>
函数有返回值：函数名括号后加上 :返回值类型

    fun randomDay():String{}   
#### 返回值：
if-else也可以设定返回值

    val temperature = 10
    val isHot = if(temperature > 50) true else false
    println(isHot) 
    >>>false
可以直接在${}使用：

    val temperature = 10
    val message = "The water temperature is ${ if (temperature > 50) "too warm" else "OK" }."
    println(message)
    >>> The water temperature is OK.

但是循环是没有值的<br>
#### 紧凑型函数：
在判断条件时使用，返回单表达式结果时省略return 和{}，直接参数值后加上 = 函数正文

    fun isHot(temperature:Int) = temperature > 30

### when
相当于switch-case<br>
语法为：

    when(variable){
        "xxx"->"xxx"
        else -> "xxx" 
    }
else部分为默认值<br>
完整代码如下：

    fun fishFood(day:String):String{
        var food  = ""
        when(day){
            "Monday" -> food = "flakes"
            "Wednesday" -> food = "redworms"
            "Thursday" -> food = "granules"
            "Friday" -> food = "mosquitoes"
            "Sunday" -> food = "plankton"
            else -> food = "bread"

        }
        return food
    }
可以修改的地方：food确保可以被赋予值，因此无需初始化。如下即可

    val food : String
还可以修改的地方为：不用food接收值，而是直接返回：

    return when (day) {
        "Monday" -> "flakes"
        "Wednesday" -> "redworms"
        "Thursday" -> "granules"
        "Friday" -> "mosquitoes"
        "Sunday" -> "plankton"
        else -> "nothing"
    }
### 过滤器
#### 即时过滤器格式为：`.fileter{predicate}` 

    val decorations = listOf ("rock", "pagoda", "plastic plant", "alligator", "flowerpot")
    println(decorations.filter { it[0]=='p' })
    // [pagoda, plastic plant]
#### 延迟过滤器：直到访问时才创建列表。格式为：`.asSequence().filter{predicate}`<br>
直接println不会访问里面的元素：

    val lazy = decorations.asSequence().filter { it[0]=='p' }
    println(lazy)
    // kotlin.sequences.FilteringSequence@7adf9f5f
用迭代器的方式才可以一个个访问：

    var it = lazy.iterator()
    while (it.hasNext()){
        println(it.next())
    }
    // pagoda
    // plastic plant
将序列转化为列表进行访问亦可：

    val newlist = lazy.toList()
    println(newlist)
    //[pagoda, plastic plant]
使用map()函数来传递里面的元素亦可

    val lazymap = decorations.asSequence().map { it }
    println(lazymap)
    //kotlin.sequences.TransformingSequence@38af3868
    println(lazymap.toList())
    //[rock, pagoda, plastic plant, alligator, flowerpot]
注意：map()函数的执行顺序是先执行transform再执行返回的函数。如下例，先执行access函数，再执行返回后的toList()函数

    val lazyMap2 = decorations.asSequence().filter {it[0] == 'p'}.map {
            println("access: $it")
            it
        }
    println("-----")
    println("filtered: ${lazyMap2.toList()}")
    //access: pagoda
    //access: plastic plant
    //filtered: [pagoda, plastic plant]
### lambda表达式：
格式：`x:type->x+2`

    var dirtyLevel = 20
    val warterFilter = {dirty:Int->dirty+10}
    //定义了一个函数，函数对象为waterFilter
    println(warterFilter(dirtyLevel))
    // 30
嵌套定义：先定义一个lambda函数
```
{ dirty -> dirty / 2 }
```
再在外面定义一个更复杂的函数
```
val waterFilter: (Int) -> 
    Int = { dirty -> dirty / 2 }
// 该代码等价于：
val dirtyChange = { dirty -> dirty / 2 }
val waterFilter(a:Int) = dirtyChange(a)
``` 
#### 高阶函数：将函数作为参数的函数或将函数作为返回值返回的函数
```
fun updateDirty(dirty:Int,operation:(Int)->Int):Int{
    return  operation(dirty)
}
```
1. operation参数定义了任意一个以Int作为参数，并返回一个Int值的函数<br>
2. UpdateDirty使用operation函数，传入dirty值进行运算，最终返回operation函数的结果
```
fun main(args:Array<String>){
   val waterFilter:(Int)->Int = {dirty->dirty/2}
    println(updateDirty(30,waterFilter))
    //15
}
```
1. waterFilter 是一个函数对象，该函数接收一个Int值，并返回一个Int值<br>
2. 其中接收的Int值作为dirty值被运算，运算后的结果作为返回的Int值<br>
3. waterFilter就符合以Int作为参数，并返回一个Int值的函数
#### 常规命名的函数作为参数传给函数，在函数名前加::即可知是传参而不是返回值
```
fun increaseDirty( start: Int ) = start + 1
println(updateDirty(15, ::increaseDirty))
//16
```


### Appendix：
- 随机数：Random().nextInt(value)返回一个从0-value-1的随机数

`return week[Random().nextInt(week.size)]`

- 使用flatten()函数可以将所有列表转化为一个列表
```
val list1 = listOf("A","B","C")
val list2 = listOf("1","2","3")
val list3 = listOf("一","二","三")
val list = listOf(list1,list2,list3)
println(list.flatten())
//[A, B, C, 1, 2, 3, 一, 二, 三]
```
