## 类和对象
### 包创建
package使代码有序<br>
名称：example.xxx
### 类创建
- 在包中创建Class<br>
- 若使用var属性定义变量，则会出现下滑线；用val则不会
> Kotlin 编码样式更加适于不可变数据，因此 IntelliJ IDEA 会将您的注意力吸引到可变数据上
- 使用val 而非 var 声明属性将不可变，并且该类的所有实例在该属性上都将具有相同的值
- 一般形式的创建：
```
class Aquarium_v1 {
    var width:Int = 20
    var height:Int = 40
    var length:Int = 100
    fun printSize(){
        println("Width:$width cm "+"Height:$height cm "+"Length:$length cm")
    }
}
```
- 非默认构造函数的创建可以直接在类定义里添加，像函数一样，也可以有默认值
```
class Aquarium(length: Int = 100,height:Int = 40,width:Int = 20){
    var length:Int = length
    var width: Int = width
    var height: Int = height
}
```
- 或者直接在括号中用var/val定义，不用再类中再定义一次（上面的方法只是类似于传参）
```
class Aquarium(var length: Int = 100, 
var width: Int = 20, 
var height: Int = 40) 
{
}
```
这样就可以直接在创建对象中指明参数进行传值了，也不需要重载构造函数
```
val myAquarium = Aquarium()
val a = Aquarium(width = 30)
val b = Aquarium(height = 40)
val c = Aquarium(height =50,length = 60)
```
- 由于不像别的语言一样有一个显式的构造函数，因此如果想要在初始化对象时进行别的操作，可以通过init{}块来执行。可以定义多个init块，执行顺序按照被定义的顺序
```
class Aquarium(var length: Int = 100,var height:Int = 40,var width:Int = 20){
    init{
        println("Now is initializing...")
        println("Hello world!")
    }
    init{
        println("The Aquarium volume is ${width * length * height / 1000} liters.")
    }
}
```
#### 次构造函数
- 此功能允许构造函数重载，不同次构造函数可以有不同的参数。
- Kotlin最好每个类只有一个主构造函数，使用默认值和命名参数
- 次构造函数必须先调用主构造函数。使用this()或间接调用
- 使用constructor定义次构造函数，:后加this()调用主构造函数的init{}
```
constructor(numberOfFish:Int):this(){
        val volume = numberOfFish*2000*1.1
        height = (volume/(length*width)).toInt()
}
```
对象定义方法：
还是类名加次构造函数中的参数。！注意，此时主构造函数和次构造函数的参数不能混着来，只能定义其中一个
```
val d = Aquarium(numberOfFish = 10)
//val e = Aquarium(width = 10,
numberOfFish = 100)
//Cannot find a parameter with this name: width
```
#### getter()属性
将函数改为方法<br>
格式为：紧跟在属性定义后面的单行函数get()
```
var volume:Int = 0
    get() = width*height*length/1000
    //get()就是不顶格的
    //Property must be initialized
```
#### setter()方法
可以重新设置值
格式为：紧跟在属性定义后面的单行函数set()
```
var volume:Int
        get() = width*height*length/1000
        set(value){
        height = (value*1000)/(width*length)
    }
    //此时不能初始化了
```

### main函数创建
- 在同一个软件包下，创建Main.kt文件
- 类实例化：
```
val myAquarium = Aquarium()
myAquarium.printSize()
```
### 可见修饰符：
private protected(子类可见)
internal(模块可见)
public（默认）<br>
var可读可写可变<br>
val只可读<br>
希望自己代码可以可读可写，但是外部只可读：
```
var volume:Int
    get() = width*height*length/1000
    private set(value){height = value*1000/(width*length)}
```
### 子类和继承
#### 创建子类
- 首先将父类open：open class,open 所有属性,open所有方法
- 子类如果要替换父类的属性或者方法，需要override关键字
`class TowerTank (override var height: Int, var diameter: Int)`
和`override var volume: Int`
- 子类调用父类的构造函数，在自己的构造函数之后
`class TowerTank (override var height: Int, var diameter: Int): Aquarium(height = height, width = diameter, length = diameter) {}`
### 抽象类和接口
#### 抽象类的创建
- 抽象类无法实例化，但可以有构造函数
- 始终开放，不用显式open；也可以用abstract显式标记某些非抽象类
- 子类必须实现抽象类的属性和方法，实现时要用override
- 继承抽象类的方法也是在冒号后调用抽象类构造函数
```
abstract class AquariumFish(){
    abstract val color:String
}
class Shark:AquariumFish(){
    override val color: String = "grey"

}
```
#### 接口的创建
- 语法格式：
```
interface FishAction{
    fun eat()
}
```
- 使用接口后也要用override来标明重写
- 使用：同样在冒号后面；如果既要继承又要接口，则逗号隔开
```
class Shark:AquariumFish(),FishAction{
    override fun eat() {
        println("hunt and eat fish")
    }
}
```
#### 区别：
- 可以在一个类中实现多个接口，但只能从一个类创建子类，即不允许多继承
#### 接口委托
接口方法由助手类实现，而想要用该接口的方法都使用助手类的实例
- 单例类（助手类）的创建：
因为创建多个助手类实例无意义，因此只是用object来创建即可，该实例由类名称引用：
```
object GoldColor : FishColor {
   override val color = "gold"
}
```
- 助手类的使用：格式：在子类原本写接口的地方： 接口  by 助手类，意思为：每次访问color系统都会将其委托给 GoldColor，而不是实现FishColor
```
class Plecostomus:  FishColor by GoldColor {}
```
- 如果该种鱼有多种颜色呢：将fishColor作为构造函数参数，默认值为GoldColor，委托变成了by 参数
```
class Plecostomus(fishColor: FishColor = GoldColor): 
       FishColor by fishColor {}
```
### 数据类
- 在class前使用data修饰
`data class Decoration(val rocks: String) {}`
- 输出结果：
`Decoration(rocks=granite)`
- 数据类对象的复制是对象引用的复制而不是内容的复制。如果是内容的复制要使用copy()



### Appendix
- 对数据类对象使用 == 等同于使用equals()
- 检查引用相同，用===
- unpack：
如同python一样，对数据类对象属性的引用可以使用打包和解包：
```
data class Decoration(val rocks:String,val wood:String,val diver:String) {}
val decoration1 = Decoration("crystal", "wood", "diver")
val (rocks,wood,diver) = decoration1
```
### 枚举类
- enum class创建，可以通过对象名.属性名进行值的引用，还可以用 对象名.属性名.name表示枚举名称；对象名.属性名.ordinal表示枚举序数
```
enum class Color(val rgb:Int){
    RED(0xFF0000),GREEN(0x00FF00),BLUE(0x0000FF)
}
fun main() {
    println(Color.BLUE.rgb)
    println(Color.BLUE.ordinal)
    println(Color.BLUE.name)
}
//255
//2
//BLUE
```
### 伴生对象
- 类似于static，使用与类关联的函数或属性而不是与实例关联
- 格式：外面是正常定义类，里边套上companion object{}
```
class Choice{
    companion object{
        var name:String = "lyric"
        fun showDescription(name:String) = println("My favourite $name")
    }
}
```
- 使用：类名.属性/函数名
```
println(Choice.name)
Choice.showDescription("pick")
```
### 对和三元组

#### 创建对
格式：to链接两个值 .first .second引用每个值
```
val equipment = "fish net" to "catching fish"
println("${equipment.first} used for ${equipment.second}")
//fish net used for catching fish
```
#### 创建三元组
格式：Triple()创建三元组，.first .second .third引用每个值
```
val number = Triple(6,9,12)
println(number.toString())
println(number.toList())
println(number.third)
//(6, 9, 12)
//[6, 9, 12]
//12
```
- 对和三元组之间的元素可以是不同类型的
```
val equipment2 = ("fish net" to "catching fish") to "equipment"
println("${equipment2.first} and ${equipment2.second}")
//(fish net, catching fish) and equipment
```
- 对和三元组可以unpack
```
val equipment = "fish net " to "catching fish"
val (tool,use) = equipment
println("${tool} is used to ${use}")
//fish net  is used to catching fish
```
### Collections
#### 列表
- 分为List 和 MutableList
- 方法：Add remove reversed contains subList
- sum()：数字可以直接计算；如果是不知道如何计算的，可以使用sumBy(lambda函数)
```
val list2 = listOf("a","bbb","cc")
println(list2.sumBy{it.length})
//6
```
- 列表的遍历：listIterator
```
val list2 = listOf("a","bbb","cc")
for (s in list2.listIterator()){
     println("$s ")
}
//a bbb cc
```
#### hashMap
- 创建：hashMapOf(key to value)，即使用to的方式创建键值对
- 搜索：get(key)，若没有这个key则返回null；getOrDefault(key,String)，找不到则返回Default的值；使用getOrElse(key){sentence}则可以找不到执行一些语句
```
val sci = hashMapOf("1" to "Messi","2" to "Curry","3" to "Haru")
println(sci.get("1"))
//Messi
```
### 整理和定义常量
#### 常量的定义：
- 格式：`const val x = 1`
- 一经编译无法更改，在编译阶段值即确定，不可再赋值
- val可以在运行时由函数返回值赋值；而const val不可以
- const val只能在顶层或者object单例对象中使用
```
object Constants{
    const val rocks = 1
}
println(Constants.rocks)
```
- 也可以在伴生对象中使用：
```
class Constants{
    companion object{
        const val rocks = 1
    }
}
println(Constants.rocks)
```
### 拓展函数
- 拓展函数可以在类的基础上添加函数，但不会修改原有类
- 格式：原类.新函数/新属性
```
fun String.hasSpaces():Boolean{
    val found = this.indexOf(' ')
    return found != -1
}
```
```
val AquariumPlant.isGreen: Boolean
   get() = color == "green"
```
- 扩展函数不能访问类的private成员
```
class Constants(val color:String,private val size:Int){
}
fun Constants.print(){
    println(size)
    //Cannot access 'size': it is private in 'Constants'
} 
```
- 拓展函数在编译时就静态解析了
- 可为null的接收器：
格式：?
```
fun AquariumPlant?.pull() {
   this?.apply {
       println("removing $this")
   }
}
// this?.apply就是测试this是否为null，若为null则不执行
```