### 构建首个应用
- Package name 是 Android 系统用来唯一标识您的应用的名称。通常，该名称默认为贵组织的名称后跟应用的名称，且所有字符均采用小写形式
- Minimum SDK 指示可运行您的应用的最低 Android 版本
### 布局
viewGroup：Layout，包含view
View：有TextView和Button等
### Activity
- 顶级：MainActivity。MainActivity中没有main函数，第一次打开应用时会调用OnCreate()而不是main()<br>
通过setContentView()设置起始布局
- 为Button设置事件
在布局中找到该Button：使用findViewById(资源ID)，资源ID可以通过activity_main.xml找到。将Button对象的引用保存而不是对象本身。
```
val rollButton: Button = findViewById(R.id.button)
```
- 资源ID：`R.<type>.<name>` 例如：`R.string.roll``R.id.button`
- 设置监听器：button对象.setOnClickListener{lambda}，lambda为点击后要执行的语句
- Toast：向用户显示的简单消息
创建包含文字的Toast并自行显示
```
val toast = Toast.makeText(this,"Dice Rolled!",Toast.LENGTH_SHORT)
//Toast.LENGTH_SHORT是指文字的持续时间：Short 2s；LONG 3.5s
toast.show()
```

### Appendix
- 从一个序列中取一个随机数：
```
 return (1..x).random()
```