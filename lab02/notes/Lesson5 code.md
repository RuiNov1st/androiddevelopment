## Layouts
#### 约束条件的添加：
```
layout_constraint<Source>_to<Target>Of
// <source>指当前view
//<target>指当前view被约束到的目标view
```
- 无法在constraintLayout下设置match_parent。可以将宽度设置为0dp，告知系统不计算宽度，只尝试匹配针对视图设置的约束条件即可。然后左边start设置为parent的Start，右边的end设置为parent的end

### 视图绑定
- 修改grandle文件
- 顶级变量 `binding: ActivityMainBinding`声明
- 访问布局中的views：
`binding = ActivityMainBinding.inflate(layoutInflater)`
- 设置内容视图为binding.root

### RecycleView
- 组件大致分工：<br>
列表项 - 列表的一个数据项。
Adapter - 获取数据并准备数据以供 RecyclerView 显示。<br>
ViewHolder - 供 RecyclerView使用的视图池。<br>
RecyclerView - 屏幕上的视图。<br>
- 布局中只有一个子视图时，可以使用FrameLayout
- 布局管理器：以什么方式显示列表项-线性则为LinearLayoutManager
`app:layoutManager="LinearLayoutManager"`
- 竖直方向上滚动条：`android:scrollbars="vertical"`
#### Adapter：
将数据调整为可供RecycleView使用的形式
（详见Lab02 recyclerView Note.pdf）


### Appendix：
- numberDecimal：带小数点的数字
- 设置占位符：在srings.xml中设置%s。在activity中应用时`getString(string.name,value)`就可以填充占位符了