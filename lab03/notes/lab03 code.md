### fragment
Activity界面中的一种行为或某一部分
- fragment在Kotlin类中定义
在onCreateView中膨胀fragment视图</br>
activity中在onCreate()中膨胀布局

```
override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // 膨胀fragment视图：
        // params: 绑定布局的inflater,布局xml 父级ViewGroup, attachtoParent?
        val binding = DataBindingUtil.inflate<FragmentTitleBinding>(inflater,R.layout.fragment_title,container,false)
        return binding.root
    }
```
- Frament界面在XML布局文件中定义。
```
<fragment />
```
### navigation:
- 在gradle的build.gradle中添加navigationVersion变量和navigation-fragment/ui-ktx的依赖
- 导航宿主fragment：导航宿主 fragment 会根据需要换入或换出 fragment
- 导航宿主xml：
```
<fragment
            android:id="@+id/myNavHostFragment"
            android:name="androidx.navigation.fragment.NavHostFragment"
            android:layout_width="match_parent"
            android:layout_height="match_parent"
            app:navGraph ="@navigation/navigation"
            app:defaultNavHost = "true"/>
```
- 创建导航图：在navigation.xml中添加fragment然后建立箭头联系
- 创建导航方式：在department fragment中绑定相关按钮并导航到Destination：
```
binding.playButton.setOnClickListener { view : View ->
       view.findNavController().navigate(R.id.action_titleFragment_to_gameFragment)
}
```
- 更改返回目的地：
导航线上的pop Behavior
- 向上按钮：返回导航前的屏幕：
在MainActivity.kt中：
```
override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        @Suppress("UNUSED_VARIABLE")
        val binding = DataBindingUtil.setContentView<ActivityMainBinding>(this, R.layout.activity_main)
        val navController = this.findNavController(R.id.myNavHostFragment)
        NavigationUI.setupActionBarWithNavController(this,navController)

    }

    override fun onSupportNavigateUp(): Boolean {
        val navController = this.findNavController(R.id.myNavHostFragment)
        return navController.navigateUp()
    }
    }
```
- 菜单项资源：
分为菜单项界面和菜单资源<br>
菜单项fragment为菜单项界面<br>
菜单项资源需要新建menu resource file，添加menu的id与菜单项fragment相同<br>
添加Onclick处理程序，在要添加菜单项的fragment处：
`setHasOptionsMenu(true)`<br>
函数onCreateOptionsMenu()：添加菜单项资源文件：
```
override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater.inflate(R.menu.options_menu, menu)
}
```
函数onOptionsItemSelected()：点击菜单项时的操作，如导航至与选定菜单项有相同id的fragment
```
override fun onOptionsItemSelected(item: MenuItem): Boolean {
     return NavigationUI.
            onNavDestinationSelected(item,requireView().findNavController())
            || super.onOptionsItemSelected(item)
}
```
### 抽屉式导航：
- 添加依赖：
`implementation "com.google.android.material:material:$supportlibVersion"`
- 设置目的fragment
- 创建导航栏资源：resource file:menu，添加多个menu item，设置menu item的id与目的地fragment相同
- 在需要导航栏的布局文件中：将drawerlayout作为根视图并添加navigationView使用刚才的menu资源文件：
```
<layout xmlns:android="http://schemas.android.com/apk/res/android"
   xmlns:app="http://schemas.android.com/apk/res-auto">
   <androidx.drawerlayout.widget.DrawerLayout
       android:id="@+id/drawerLayout"
       android:layout_width="match_parent"
       android:layout_height="match_parent">

   <LinearLayout
       . . .
       </LinearLayout>
    <com.google.android.material.navigation.NavigationView
   android:id="@+id/navView"
   android:layout_width="wrap_content"
   android:layout_height="match_parent"
   android:layout_gravity="start"
   app:headerLayout="@layout/nav_header"
   app:menu="@menu/navdrawer_menu" />
   </androidx.drawerlayout.widget.DrawerLayout>
</layout>
```
- 滑动显示导航栏：
mainActivity中：
`NavigationUI.setupWithNavController(binding.navView,navController)`
- 点击显示导航栏：
将drawerLayout加入setupActionBarWithNavController()的参数中<br>
返回navigateUp
### Bundle传递数据
Bundle类为键值对
- 使用safe Args插件：在project build.gradle文件中加入依赖
`classpath "androidx.navigation:navigation-safe-args-gradle-plugin:$navigationVersion"`
在module build gradle文件中加入：`apply plugin: 'androidx.navigation.safeargs'`
- 在要传参的出发fragment中设置navigate：使用Directions类，后面接着要传递的参数
```
view.findNavController().navigate(GameFragmentDirections.actionGameFragmentToGameWonFragment(numQuestions,questionIndex))
```
- 参数设置：到navigation.xml中设置接收Fragment的Arguments
- 提取参数：到接收fragment中用bundle提取参数，并可以用toast显示
```
val args = GameWonFragmentArgs.fromBundle(requireArguments())
Toast.makeText(context, "NumCorrect: ${args.numCorrect}, NumQuestions: ${args.numQuestions}", Toast.LENGTH_LONG).show()
```
### Intent启动外部app
- 显式（特定App）和隐式（所有具有该功能的App）
- 每个隐式的intent都必须具有ACTION描述待完成的操作
```
al shareIntent = Intent(Intent.ACTION_SEND)
shareIntent.setType("text/plain").putExtra(Intent.EXTRA_TEXT,getString(R.string.share_success_text))
```
- startActivity方法可以根据Intent调用分享
`startActivity()`
- 设置分享菜单：
```
override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
    super.onCreateOptionsMenu(menu, inflater)
    inflater.inflate(R.menu.winner_menu,menu)
    //检查是否能够解析为Activity:
    //如果不能，则设置分享菜单项不可见
    if(getShareIntent().resolveActivity(requireActivity().packageManager)==null){
        menu.findItem(R.id.share).isVisible  =false
    }
}
```
- 设置点击菜单中的按钮后进行分享：
```
override fun onOptionsItemSelected(item: MenuItem): Boolean {
      when(item.itemId){
          R.id.share->shareSuccess()
      }
      return super.onOptionsItemSelected(item)
  }
```
### Appendix：
- 在navigation的2.3.0版本中需要使用java8特性。需要在build.gradle中添加：
```
android {
    ...
    // Configure only for each module that uses Java 8
    // language features (either in its source code or
    // through dependencies).
    compileOptions {
        sourceCompatibility = JavaVersion.VERSION_1_8
        targetCompatibility = JavaVersion.VERSION_1_8
    }
    // For Kotlin projects
    kotlinOptions {
        jvmTarget = "1.8"
    }
}
```
