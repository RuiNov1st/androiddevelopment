### Lesson 9
---
#### 创建room数据库
- 数据类实体：
```
@Entity(tableName = "daily_sleep_quality_table")
data class SleepNight(
    @PrimaryKey(autoGenerate = true) //给nightId添加为主键，自动生成实体ID，保证每晚ID唯一
    var nightId:Long = 0L,
    @ColumnInfo(name = "start_time_milli")//使用自定义属性名称
    val startTimeMilli:Long = System.currentTimeMillis(),
    @ColumnInfo(name = "end_time_milli")
    var endTimeMilli:Long = startTimeMilli,
    @ColumnInfo(name = "quality_rating")
    var sleepQuality:Int = -1
)
```
- 创建DAO：
直接用@Insert注解即可，Room自动生成：
```
@Insert
fun insert(night: SleepNight)
@Update
fun update(night: SleepNight)
@Query("SELECT * From daily_sleep_quality_table WHERE nightId = :key")
fun get(key:Long):SleepNight?
```
- 创建数据库
```
//用于执行 extends RoomDatabase 操作的类充当数据库容器。
// 抽象类，因为Room会创建实现。
//参数： entities：实体列表；Version：每更改框架时都要增加版本号；exportSchema：不会保留架构版本记录的备份
@Database(entities = [SleepNight::class],version = 1,exportSchema = false)
abstract class SleepDatabase :RoomDatabase(){
    //知悉DAO
    abstract val sleepDatabaseDao:SleepDatabaseDao
    //伴生对象：允许客户端访问以创建和获取数据库，无需实例化
    companion object{
        //在数据库创建后保留对数据库的引用
        @Volatile // 不会缓存，在主内存中完成，且对所有执行线程相同
        private var INSTANCE:SleepDatabase? = null
        //提供数据库构建器所需的Context
        fun getInstance(context: Context):SleepDatabase{
            //只有一个线程可以进入此代码块，数据库仅初始化一次
            synchronized(this){
                var  instance = INSTANCE
                //如果现在还没有数据库
                if (instance==null){
                    //使用数据库构建器获取数据库，传入上下文、数据库类以及数据库名称
                        //迁移策略：如何获取旧架构的所有行并将其转换为新架构中的行，使数据不会丢失
                    instance = Room.databaseBuilder(context.applicationContext,SleepDatabase::class.java,"sleep_history_database").fallbackToDestructiveMigration().build()
                    INSTANCE = instance
                }
                return instance

            }
        }
    }

}
```
---
#### 使用DAO数据库：
- 使用ViewModel：
```
//获取对应用上下文的引用。
// 引用此fragment连接到的应用
val application = requireNotNull(this.activity).application

//引用DAO引用数据源
val dataSource = SleepDatabase.getInstance(application).sleepDatabaseDao

//创建viewModelFactory实例
val viewModelFactory = SleepTrackerViewModelFactory(dataSource,application)
//获取SleepTrackerViewModel的引用
//which will create ViewModels via the given Factory and retain them in a store of the given ViewModelStoreOwner.
val sleepTrackerViewModel = ViewModelProvider(this,viewModelFactory).get(SleepTrackerViewModel::class.java)
return binding.root
```
---
#### 协程
- 特性：异步且非阻塞式的；可被挂起到后台运行
- 要素：作业-每个协程都有一项作业；调度程序-发出协程在各种线程上运行；作用域-定义协程在什么上下文中运行
- 关键字：suspend
- 协程的启动：
```
viewModelScope.launch {}
```